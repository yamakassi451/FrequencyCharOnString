package ru.yamakassi.frequencycharonstring.config;


import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class OpenAPIConfig {

    @Bean
    public OpenAPI myOpenAPI() {
        Contact contact = new Contact()
                .name("Ivan Afanasev")
                .email("yamakassi451@gmail.com");

        Info info = new Info()
                .title("Frequency App API")
                .description("API for calculating character frequency in a given string.")
                .version("1.0.0")
                .contact(contact);

        return new OpenAPI().info(info);
    }



}
