package ru.yamakassi.frequencycharonstring.service;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class FrequencyCalcService {

    public LinkedHashMap<Character, Integer> calculateFrequency(String inputString) {
        // Вычисляем частоту
        Map<Character, Integer> charFrequency = new HashMap<>();
        for (char c : inputString.toCharArray()) {
            charFrequency.put(c, charFrequency.getOrDefault(c, 0) + 1);
        }

        // Сортируем результат по убыванию частоты
        return charFrequency.entrySet().stream()
                .sorted((entry1, entry2) -> entry2.getValue().compareTo(entry1.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }
}
