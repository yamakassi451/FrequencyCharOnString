package ru.yamakassi.frequencycharonstring.controller;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.yamakassi.frequencycharonstring.model.request.FrequencyCalcRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import ru.yamakassi.frequencycharonstring.model.response.CharFrequencyResponse;
import ru.yamakassi.frequencycharonstring.service.FrequencyCalcService;

import java.util.LinkedHashMap;
import java.util.Map;

@RestController
public class FrequencyCalculatorController {

    private final FrequencyCalcService calculatorService;

    public FrequencyCalculatorController(FrequencyCalcService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @Operation(summary = "Вычислить частоту символов", description = "Метод вычисляет частоту встречи символов в заданной строке и возвращает результат в убывающем порядке.")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(
            content = @Content(
                    schema = @Schema(example = "{\n" +
                    " \"inputString\": \"aaabcaaccc\"\n" +
                    "}")))
    @ApiResponse(
            responseCode = "200",
            description = "Успешный запрос",
            content = @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = LinkedHashMap.class),
                    examples = @ExampleObject(
                            name = "sampleResponse",
                            value = "{\"a\": 5, \"c\": 4, \"b\": 1}"
                    )
            )
    )
    @PostMapping("/calc/frequency")
    public ResponseEntity<CharFrequencyResponse> calcFrequency(@Validated @RequestBody FrequencyCalcRequest request) {
        String inputString = request.getInputString();
        LinkedHashMap<Character, Integer> sortedCharFrequency = calculatorService.calculateFrequency(inputString);
        CharFrequencyResponse response = new CharFrequencyResponse(sortedCharFrequency);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
