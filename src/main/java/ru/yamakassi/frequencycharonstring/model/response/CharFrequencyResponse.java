package ru.yamakassi.frequencycharonstring.model.response;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.Objects;

@Getter @Setter
public class CharFrequencyResponse {
    private Map<Character, Integer> data;

    public CharFrequencyResponse(Map<Character, Integer> data) {
        this.data = data;
    }

    public Map<Character, Integer> getData() {
        return data;
    }


}
