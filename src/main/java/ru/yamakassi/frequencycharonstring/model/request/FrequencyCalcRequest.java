package ru.yamakassi.frequencycharonstring.model.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
@Getter @Setter
public class FrequencyCalcRequest {
    @Size(max = 1000, message = "Строка должна быть не больше 1000 символов")
    @Pattern(regexp = "^[a-z0-9]+$", message = "На вход только английские буквы и цифры")
    private String inputString;
}
