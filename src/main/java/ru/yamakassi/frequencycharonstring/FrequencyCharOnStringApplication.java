package ru.yamakassi.frequencycharonstring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrequencyCharOnStringApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrequencyCharOnStringApplication.class, args);
	}

}
