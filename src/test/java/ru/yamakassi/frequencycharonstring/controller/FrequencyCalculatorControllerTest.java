package ru.yamakassi.frequencycharonstring.controller;



import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.yamakassi.frequencycharonstring.model.request.FrequencyCalcRequest;
import ru.yamakassi.frequencycharonstring.model.response.CharFrequencyResponse;
import ru.yamakassi.frequencycharonstring.service.FrequencyCalcService;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;

public class FrequencyCalculatorControllerTest {

    private MockMvc mockMvc;

    @Mock
    private FrequencyCalcService calculatorService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        FrequencyCalculatorController controller = new FrequencyCalculatorController(calculatorService);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }


    @Test
    public void testCalcFrequency_ValidInput() throws Exception {
        FrequencyCalcRequest request = new FrequencyCalcRequest();
        request.setInputString("aaabcaaccc");

        // Ожидаемый результат
        LinkedHashMap<Character, Integer> expectedResult = new LinkedHashMap<>();
        expectedResult.put('a', 5);
        expectedResult.put('b', 1);
        expectedResult.put('c', 4);
        CharFrequencyResponse res = new CharFrequencyResponse(expectedResult);
        when(calculatorService.calculateFrequency(request.getInputString())).thenReturn(expectedResult);

        String expectedJson = asJsonString(res);

        mockMvc.perform(post("/calc/frequency")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(result -> {
                    String actualJson = result.getResponse().getContentAsString();
                    JSONAssert.assertEquals(expectedJson, actualJson, false);
                });

        verify(calculatorService, times(1)).calculateFrequency(request.getInputString());
        verifyNoMoreInteractions(calculatorService);
    }

    @Test
    public void testCalcFrequency_MaxInputLengthExceeded() throws Exception {
        StringBuilder longInput = new StringBuilder();
        for (int i = 0; i < 1001; i++) {
            longInput.append("a");
        }
        FrequencyCalcRequest request = new FrequencyCalcRequest();
        request.setInputString(longInput.toString());

        mockMvc.perform(post("/calc/frequency")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request)))
                .andExpect(status().isBadRequest());
        verifyNoInteractions(calculatorService);
    }

    @Test
    public void testCalcFrequency_NonEnglishCharacters() throws Exception {
        FrequencyCalcRequest request = new FrequencyCalcRequest();
        request.setInputString("Тест123");

        mockMvc.perform(post("/calc/frequency")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request)))
                .andExpect(status().isBadRequest());
        verifyNoInteractions(calculatorService);
    }
    @Test
    public void testCalcFrequency_Uppercase() throws Exception {
        FrequencyCalcRequest request = new FrequencyCalcRequest();
        request.setInputString("AAAf");

        mockMvc.perform(post("/calc/frequency")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request)))
                .andExpect(status().isBadRequest());
        verifyNoInteractions(calculatorService);
    }

    @Test
    public void testCalcFrequency_EmptyInput() throws Exception {
        FrequencyCalcRequest request = new FrequencyCalcRequest();
        request.setInputString("");

        mockMvc.perform(post("/calc/frequency")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request)))
                .andExpect(status().isBadRequest());
        verifyNoInteractions(calculatorService);
    }

    // Метод для преобразования объекта в JSON-строку
    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
